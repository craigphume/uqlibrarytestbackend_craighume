<?php
/**
 * Created by PhpStorm.
 * User: chume
 * Date: 16/11/16
 * Time: 11:13 PM
 */

namespace App\Http\Middleware;

use Closure;

class ValidUser
{

    public function handle($request, Closure $next)
    {
        if(!$request->header('X-VALID-USER'))
        {
            abort(401);
        }
        return $next($request);
    }
}