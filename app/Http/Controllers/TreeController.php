<?php
/**
 * Created by PhpStorm.
 * User: chume
 * Date: 17/11/16
 * Time: 9:15 PM
 */

namespace app\Http\Controllers;

use Illuminate\Http\Request;

class TreeController
{

    /**
     * @param Request $request
     * @return mixed|null
     */
    public function find(Request $request) {

        // Guard against there being no tree parameter
        if($request->has('tree')) {
            // Decode the tree from JSON to associative array
            $tree = json_decode($request->input('tree'), true);

            // Check if the tree is not single node
            if (count($tree) > 1) {
                // Call recursive function passing in the tree array
                return $this->getLeafNode($tree);
            }
            // Return the root of the single node tree
            return $tree['root'];
        }
        abort(422);
    }

    /**
     *
     * Recursive function to find the lowest leaf value of the tree
     * It does not define which node contains the lowest and as such
     * duplicates are given equal weighting
     * @param $branch
     * @param null $low
     * @return mixed|null
     */
    function getLeafNode($branch, $low = null) {
        // If it is an array it will have 2 or less child nodes
        if(is_array($branch)) {
            // Iterate over n branches
            foreach ($branch as $subBranch) {
                // If its not an array at this level then it's a root parent node
                // not a leaf node
                if(is_array($subBranch))
                {
                    // If the branch has only one child then it is a leaf node
                    if(count($subBranch) == 1)
                    {
                        // Assign the root node value to the $res (result)
                        // temp variable
                        $res = $subBranch['root'];
                        // Compare the lowest value seen so far with the new
                        // value or if this is the first iteration $low will
                        // be null
                        if($res < $low || is_null($low))
                        {
                            $low = $res;
                        }
                    }
                    // Recurse again passing in the next level of branch
                    // and the current lowest value
                    $low = $this->getLeafNode($subBranch, $low);
                }
            }
        }
        return $low;
    }
}