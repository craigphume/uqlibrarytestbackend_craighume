<?php

namespace App\Http\Controllers;
use App\Library;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LibraryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $id
     */
    public function show($id) {

        // Using where to find the data using 'id' which is not the primary key in this case
        // therefore cant use findOrFail($id) or ::find($id)
        try {
            return Library::where('id', $id)->firstOrFail();
        }catch (Exception $e){
            abort(404);
        }
    }


    /**
     * @param Request $request
     */
    public function store(Request $request) {

        // If the parameter library is missing abort and return the error to the requester
        if(!$request->has('library'))
        {
            abort(422, 'Missing library parameter');
        }

        // Decode the library json into associative array
        $library = json_decode($request->input('library'), true);

        // Validate the incoming library request
        // I have made a judgment call on saving duplicate library id's and
        // require them to be unique
        $val = Validator::make($library, [
            'id' => 'required|unique:libraries|integer|min:0',
            'code' => 'required|regex:[^([A-Za-z]{3})([0-9]{3})$]',
            'name' => 'required|string',
            'abbr' => 'required|string',
            'url' => 'required|url',
        ]);

        // Abort if the validation fails and return the error to the requester
        if($val->fails())
        {
            abort(422, $val->errors());
        }

        // save the response to the database
        Library::create($library);

    }
}
