<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

// Add Group Prefix to house the api route
$app->group(['prefix' => 'api'], function () use ($app) {
    // Add another Group Prefix to house the library route
    $app->group(['prefix' => 'library'], function () use ($app) {
        $app->get('{id}', 'LibraryController@show');
        $app->group(['middleware' => 'validUser'], function () use ($app) {
            $app->post('/', 'LibraryController@store');
        });
    });

    $app->get('findSmallestLeaf', 'TreeController@find');
});
