<?php
/**
 * Created by PhpStorm.
 * User: chume
 * Date: 16/11/16
 * Time: 9:22 PM
 */
namespace App;


use Illuminate\Database\Eloquent\Model;

class Library extends Model
{

    protected $table = 'libraries';
    protected $primaryKey = 'idLibrary';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code', 'name', 'abbr', 'url',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'idLibrary', 'created_at', 'updated_at',
    ];
}
