<?php

use Illuminate\Database\Seeder;
use App\Library as Library;

class LibrariesTableSeeder extends Seeder {

    public function run()
    {
        // Add library for testing the route
        Library::create([
            'id' => 10123,
            'code' => 'ARC100',
            'name' => 'Architecture / Music Library',
            'abbr' => 'Arch Music',
            'url' => 'http://www.library.uq.edu.au/locations/architecture-music-library'
        ]);
    }
}