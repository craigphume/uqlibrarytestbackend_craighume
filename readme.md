# UQLibrary Work Test Answers
## Craig Hume - Documentation

### Overview, assumptions and decisions
Backend test: PHP Web Service

I chose to write this section using Lumen as I have experience Laravel and 
Lumen being a subset of, it seemed appropriate.

For this section I allocated 9 hours total, 5 for coding:

* 1 hour design and app setup (DB, configuration and routes)
* 2 hours for *GET /api/library/{id}* and *POST /api/library/*
* 2 hours for *GET /api/findSmallestLeaf*

* 3 hours for Documentation
* 1 hours server testing

The actual time spent was closer to 9 hours coding.
A break down of the time over run is below.

Routing in lumen by default doesn't use the Facades as in Laravel which having 
not used Lumen before took time to check the documentation before 
implementation.

Application Setup is stored in a single app.php in Lumen and not in separate 
files as in Laravel, again this is down to lack of framework knowledge

Switching to Lumen cost 1 hour extra.

Major part of time estimate over run was implementing findSmallestLeaf function.
I attempted to use SPL functions to walk the tree, but the design of the tree 
did not work well with the Iterators and array_walk_recursive as I was getting 
all node values and not just leaf values. Therefore I implemented my own 
recursive walker to find the leaf nodes. But by doing so I overrun my estimates 
for this section by 3 hours.

Total time for this project
* 1 hour design and app setup
* 3 hours for *GET /api/library/{id}* and *POST /api/library/*
* 5 hours for *GET /api/findSmallestLeaf*

* 3 hours for Documentation
* 1 hours server testing

### Installation
Installation was tested in a VM running Ubuntu 16.04 Server and the LAMP meta 
package selected during install time.

Installation can also be used with Homestead by following the instructions here
 https://laravel.com/docs/5.3/homestead

If you have php, composer and git installed locally then running 
*php -S localhost:8080 -t public/* in the project root will allow local testing 
of the server

##### Server Installation Setup
* Install dependencies
  * ``apt install php7.0-mbstring php7.0-xml php7.0-sqlite3 unzip zip``
* Install composer
  * ``curl -sS https://getcomposer.org/installer | php``
  * ``sudo mv composer.phar /usr/local/bin/composer``
* Clone repository
  * ``git clone https://bitbucket.org/craigphume/uqlibrarytestbackend_craighume.git``
* Add your user to www-data group
  * ``sudo usermod -a -G www-data user``
* Copy to http dir
  * ``cp -R ~/test1/uqlibrarytestbackend_craighume/* /var/www/http/``
* Set ownership and permissions
  * ``sudo chgrp -R www-data /var/www/html/``
  * ``sudo chmod -R 755 /var/www/html/storage``
* Create database
  * ``touch /var/www/html/database/database.sqlite``
* Run Composer
  * ``composer install``
* Restart apache
  * ``sudo service apache2 restart``

The server should be up and running


### Database
The database is a SQLITE db with a table library for the persistence of the 
library data.

### Routes
Routes are wrapped into route groups which define the api 
prefix and a second which defines the library. I left the findSmallestLeaf out 
of the Library prefix.

### Controllers
Two controllers were created to handle the routes, 

*LibraryController.php* contains all business logic for the retrieval and 
persistence of libraries.
*TreeController.php* contains all the logic to parse the tree, search the tree 
and return the data back to the caller.

##### LibraryController.php
The *store* function is the endpoint for *GET /api/library/{id}*. As per the 
database schema, the search is performed on the *library* table using the WHERE 
eloquent facade. This is because the FIND tries to match the primary key of the 
table which ID is not. If the data is not found an exception is thrown and 
caught in the catch block.
The appropriate 404 status is returned to the caller.

The *show* function is the endpoint for *POST /api/library/*. This route is 
different as it has a middleware guard which is used to validate the 
X-VALID-USER token which should be set in the HTTP Header.
This function has performs three important tasks
+ Checks for the existence of the *library* parameter
+ Decode and validate the JSON
+ Persists the data

Validation utilises lumen's builtin validator, the two validations of note are 
below:

The *id* parameter is validated for uniqueness in the library table, and 
also checked that it's a positive integer.

The *code* parameter is validated using a regex the ensures it is in the 
correct XXXYYY (3x char and 3x number) format as per the specification.
 
Should the validation fail, a 422 - Unprocessable Entity HTTP 
Status code and message is returned, as it was deemed the most appropriate in 
this case.
  

##### TreeController.php
The *find* function is the endpoint for *Get /api/findSmallestLeaf*. Firstly it 
checks for the existence of the tree parameter, if not the appropriate 
422 - Unprocessable Entity HTTP Status code and message is returned.

The tree is then decoded into an associative array and send into the 
*getLeafNode* recursive function for processing.

The *getLeafNode* function finds the lowest value on leaf nodes.
This is accomplished by iterating over the current branch checking for further 
arrays and checking the count of the array because if the array count is 1 then 
the root value contained is the leaf node, which is then set to the $low 
variable and passed to the next pass of the recursion.

This checks for the currently lowest value and returns the lowest value back as
the function exits.

The lowest leaf node is returned to the caller.

#### Middleware
The middleware file ValidUser.php was created to check for the X-VALID-USER 
header parameter. It does not check for an appropriate token value as per the 
specification.

If the header is not found or empty it will return the 401 - Forbidden HTTP 
Status Code as per the specification.

The Middleware is registered in app.php as a routeMiddleware and applied to the 
route */api/findSmallestLeaf* only.

